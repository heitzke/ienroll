/* fires modal */
$('.login').on('click', function(e) {
  e.preventDefault();
  $('#login').modal();
});

/* fires terms modal */
$('.terms').on('click', function(e) {
  e.preventDefault();
  $('#terms').modal();
});

/* fires policy modal */
$('.privacy').on('click', function(e) {
  e.preventDefault();
  $('#privacy').modal();
});

/* fires contact modal */
$('.contact').on('click', function(e) {
  e.preventDefault();
  $('#contact').modal();
});

function preventDefault(e) { e.preventDefault(); };

/* lazy loaded images, nav open/close stuff */
$(document).ready(function() {
  //$('body').on('wheel.modal mousewheel.modal', function () {return false;});
  //document.addEventListener('touchmove', preventDefault, false);
  svgeezy.init(false, 'png');

  $('.downgrade').on('click', function(){
    $('#downgrade').modal('show');
    jQuery(function($) {
      $('.modal-body').bind('scroll', function(){
        if($(this).scrollTop() + $(this).innerHeight() >= (this.scrollHeight - 55)) {
          $('.btn-scroll-bottom').text('Continue').removeClass('btn-disabled');
        }
      });
    });
  });

  $('.expand-faq-content').on('click', function(e){
    $(this).closest('li').find('div').slideToggle();
  });

  $('.compare-trigger').on('click', function(e){
    $('.plans').slideUp('slow');
    $('.compare-plans').fadeIn();
  });

  $('.back-to-plans').on('click', function(e){
    $('.compare-plans').slideUp('slow');
    $('.plans').slideDown();
  });

  $('.step-2-continue').on('click', function(e){
    $('.step-1').removeClass('fadeIn').addClass('fadeOut').delay( 800 ).fadeOut();
    $('body').off('wheel.modal mousewheel.modal');
    document.removeEventListener('touchmove', preventDefault, false);
  });

  $('.change-plan').on('click', function(e){
    $('.step-3').slideUp('slow');
    $('.step-2').fadeIn();
  });

  $('.step-3-continue').on('click', function(){
    $('.step-2').slideUp('slow');
    $('.step-3').fadeIn();
  });

  $('.step-4-continue').on('click', function(){
    $('.step-3').slideUp('slow');
    $('.step-4').fadeIn();
  });
});
